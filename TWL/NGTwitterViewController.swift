//
//  NGTwitterViewController.swift
//  TWL
//
//  Created by Havic on 2/28/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//

import UIKit


class NGTwitterViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var twitterFeed = NSMutableArray()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let twitter: STTwitterAPI = STTwitterAPI(appOnlyWithConsumerKey: "aOskiBLNXxkLsFNXqRGNfXYvg", consumerSecret: "uQCIH1ifYxQ110ne302V9y80K5IrvJPRR95Hk7gxHi0deNjfs2")
        
        twitter.verifyCredentialsWithSuccessBlock({ (username) -> Void in
            
            twitter.getUserTimelineWithScreenName("techwelike", successBlock: { (statuses) -> Void in
                let statusesArray:NSArray = statuses as NSArray
                
                self.twitterFeed = statusesArray.mutableCopy() as! NSMutableArray
                self.tableView.reloadData()
                
                return
                }, errorBlock: { (error) -> Void in
                    return
            })
            }, errorBlock: { (error) -> Void in
                
                
                
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Tableview Section
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return some array .count;
        return self.twitterFeed.count;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TweetCell", forIndexPath: indexPath) 
        
        let t: AnyObject? = self.twitterFeed.objectAtIndex(indexPath.row).objectForKey("text")
       // var ty:Array = self.twitterFeed.objectAtIndex(indexPath.row).allKeys
        let avi: NSDictionary = self.twitterFeed.objectAtIndex(indexPath.row).objectForKey("user") as! NSDictionary
        let avi2: String = avi.objectForKey("profile_image_url") as! String
        let aviImg = NSURL(string: avi2)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            let imageData = NSData(contentsOfURL: aviImg!)
            
            let avitar = UIImage(data: imageData!)
            dispatch_sync(dispatch_get_main_queue(), { () -> Void in
                cell.imageView?.image = avitar
                return
            })
        })
        
        
        
        cell.textLabel?.text = t as? String
        
        // Configure the cell...
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //Configure posting info from the cell in to Twitter (if user is logged into account)
        
        if(SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)){
            let tweetSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            let cell = self.tableView.cellForRowAtIndexPath(indexPath)
            let tweetBy = " via @TechWeLike"
            let cellString = cell?.textLabel?.text
            tweetSheet.setInitialText(cellString! + tweetBy)
            presentViewController(tweetSheet, animated: true, completion: nil)
            
            
        }
        
    }
}
