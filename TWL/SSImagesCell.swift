//
//  SSImagesCell.swift
//  TWL
//
//  Created by Havic on 3/3/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//

import UIKit

class SSImagesCell: UICollectionViewCell {
    
    
    var instagramImage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Add our imageview to the cells view
        addSubview(instagramImage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //Set image views frame to size of cell
        instagramImage.frame = bounds
    }
    
}
