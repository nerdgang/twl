//
//  BlogObject.swift
//  TWL
//
//  Created by Havic on 6/25/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//

import UIKit

extension String {
  
  /**
  
  Strips extra HTML characters from String and replaces with regular characters.
  
  - returns: String
  
  */
  
  mutating func changeHtmlToString() -> String{
    var newString = self
    newString = newString.stringByReplacingOccurrencesOfString("&#038;", withString: "&")
    newString = newString.stringByReplacingOccurrencesOfString("&#8211;", withString: "-")
    newString = newString.stringByReplacingOccurrencesOfString("&#8217;", withString: "'")
    newString = newString.stringByReplacingOccurrencesOfString("&#8220;", withString: "\" ")
    newString = newString.stringByReplacingOccurrencesOfString("&#8221;", withString: "\" ")
    return newString
  }
}

class BlogObject: NSObject {
  var author: String?
  var blogTitle: String?
  var postDate: NSMutableString?
  var blogThumb:String?
  var urlString: String?
  
  init(jsonDic:NSDictionary) {
    let tempDic = jsonDic["author"] as! NSDictionary
    self.author = tempDic["name"] as? String
    self.blogTitle = jsonDic["title"] as? String
    blogTitle = blogTitle?.changeHtmlToString()
    self.postDate = jsonDic["date"] as? NSMutableString
    self.blogThumb = jsonDic["thumbnail"] as? String
    self.urlString = jsonDic["url"] as? String
  }
  
}
