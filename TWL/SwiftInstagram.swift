//
//  SwiftInstagram.swift
//  TWL
//
//  Created by Havic on 2/25/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//

import UIKit

class SwiftInstagram: NSObject {
    let accessToken = "d0ced92e2ab44a1b86d1f9144227515f" //"308696932.d0ced92.3f084a460a81401a9c9200f88630d638"
    let tag = "techwelike"
    var nextUrl = ""
    var delegate : SSSessionDelegate?
    
    //Return singleton of session object
    class var sharedInstance: SwiftInstagram {
        struct Static {
            static var instance: SwiftInstagram?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = SwiftInstagram()
        }
        return Static.instance!
    }
    
    //Get next batch of data based on either next URL or initial url.
    func getImageURLs() {
        
        if( nextUrl == "") {
            nextUrl = "https://api.instagram.com/v1/tags/" + tag + "/media/recent/?client_id=" + accessToken
        }
        
        //Set up session values to acquire data
        let defaultConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let mySession = NSURLSession(configuration: defaultConfig)
        let NSurl : NSURL = NSURL(string: nextUrl)!
        let request : NSURLRequest = NSURLRequest(URL: NSurl)
        let myDataTask = mySession.dataTaskWithRequest( request,
            completionHandler: {(data: NSData?,
                response: NSURLResponse?,
                error: NSError?) in
                
                // Serialize data, update pagination value, and send data via delegate to presenter
                let json = (try! NSJSONSerialization.JSONObjectWithData(data!, options: [])) as! NSDictionary
                let paginationDict = json.objectForKey("pagination") as! NSDictionary
                if(paginationDict.objectForKey("next_url") == nil){
                    return
                }
                self.nextUrl = paginationDict.objectForKey("next_url") as! String
                self.delegate?.acquiredImageURLs( json.objectForKey("data")as! NSArray )
                
        })
        
        myDataTask.resume()
        mySession.finishTasksAndInvalidate()
    }
    
    func getImage ( url : NSString, index : Int ){
        
        //Set up session values to acquire data
        let defaultConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let mySession = NSURLSession(configuration: defaultConfig)
        let NSurl : NSURL = NSURL(string: url as String)!
        let request : NSURLRequest = NSURLRequest(URL: NSurl)
        let myDataTask = mySession.dataTaskWithRequest(request,
            completionHandler: {(data: NSData?,
                response: NSURLResponse?,
                error: NSError?) in self
                
                //Send data via delegate to presenter
                self.delegate?.acquiredImage( data!, index: index )
        })
        
        myDataTask.resume()
        mySession.finishTasksAndInvalidate()
    }

}
