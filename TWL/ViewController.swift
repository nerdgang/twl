//
//  ViewController.swift
//  TWL
//
//  Created by Havic on 2/16/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//

import UIKit


class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,reloadJsonData {
  
  //Creating a global variable for Titles
  var blogImage = String?()
  var imageData = NSData?()
  var date = NSMutableString()
  var postTitle = String()
  var fullStory = DetailView()
  let callJson = JsonLoader()
  let refreshControl = UIRefreshControl()
  @IBOutlet weak var myTable: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    callJson.reloadJson = self
    callJson.loadThatJson() //*call to the jSON method that will handle all the downloading of the data*
    refreshControl.backgroundColor = themeColor
    refreshControl.tintColor = UIColor.whiteColor()
    refreshControl.addTarget(self, action: "handleRefresh:", forControlEvents: UIControlEvents.ValueChanged)
    myTable.addSubview(refreshControl)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  //MARK: Reload Table
  func reloadTableInfo() {
    myTable.reloadData()
    refreshControl.endRefreshing()
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return callJson.blogPostArray.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
    
    // Configure the cell...
    let blogContent = self.callJson.blogPostArray.objectAtIndex(indexPath.row) as! BlogObject
    let name = blogContent.author
    date = blogContent.postDate!
    postTitle = blogContent.blogTitle!
    cell.textLabel?.text = postTitle
    cell.detailTextLabel?.text = "\(name!) on \(formattedDate())" //author and date posted on
    cell.imageView?.image = UIImage(named: "twl100")
    
    // Multithreaded Method for downloading of the images using GCD(Grand Central Dispatch)
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
      let imageIssue = blogContent.blogThumb
      if (imageIssue != nil){
        self.callJson.blogImageUrl = NSURL(string: blogContent.blogThumb!.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)
        self.imageData = NSData(contentsOfURL: self.callJson.blogImageUrl!)
        let currentImage: UIImage = UIImage(data: self.imageData!)!
        dispatch_async(dispatch_get_main_queue(), { ()
          cell.imageView?.image = currentImage
        })
      }
    })
    return cell
  }
  
  // MARK: - Segue Method
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let storyCell = sender as! UITableViewCell
    fullStory = segue.destinationViewController as! DetailView
    let indexPathVar: NSIndexPath = myTable.indexPathForCell(storyCell)!
    let blogContent = self.callJson.blogPostArray.objectAtIndex(indexPathVar.row) as! BlogObject
    fullStory.strArticleUrl = blogContent.urlString!
  }
  
  func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    
  }
  
  //Method to format the way the date looks
  func formattedDate() -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let tempDate = dateFormatter.dateFromString(date as String)
    dateFormatter.dateFormat = "EE MMM dd, yyyy"
    return dateFormatter.stringFromDate(tempDate!)
  }
  
  //MARK: Pull to refresh
  func handleRefresh(refreshControl:UIRefreshControl){
    callJson.loadThatJson()
    }

}

