//
//  AppDelegate.swift
//  TWL
//
//  Created by Havic on 2/16/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//
/***
*    $$\   $$\                           $$\        $$$$$$\                                      $$\         $$\         $$$$$$\
*    $$$\  $$ |                          $$ |      $$  __$$\                                     $$ |        $$ |       $$  __$$\
*    $$$$\ $$ | $$$$$$\   $$$$$$\   $$$$$$$ |      $$ /  \__| $$$$$$\  $$$$$$$\   $$$$$$\        $$ |        $$ |       $$ /  \__|
*    $$ $$\$$ |$$  __$$\ $$  __$$\ $$  __$$ |      $$ |$$$$\  \____$$\ $$  __$$\ $$  __$$\       $$ |        $$ |       $$ |
*    $$ \$$$$ |$$$$$$$$ |$$ |  \__|$$ /  $$ |      $$ |\_$$ | $$$$$$$ |$$ |  $$ |$$ /  $$ |      $$ |        $$ |       $$ |
*    $$ |\$$$ |$$   ____|$$ |      $$ |  $$ |      $$ |  $$ |$$  __$$ |$$ |  $$ |$$ |  $$ |      $$ |        $$ |       $$ |  $$\
*    $$ | \$$ |\$$$$$$$\ $$ |      \$$$$$$$ |      \$$$$$$  |\$$$$$$$ |$$ |  $$ |\$$$$$$$ |      $$$$$$$$\$$\$$$$$$$$\$$\$$$$$$  |$$\
*    \__|  \__| \_______|\__|       \_______|       \______/  \_______|\__|  \__| \____$$ |      \________\__\________\__\______/ \__|
*                                                                                $$\   $$ |
*                                                                                \$$$$$$  |
*                                                                                 \______/
*/

import UIKit

import TwitterKit


//Global variables for the color schemes of the app

let themeColor = UIColor(red: 56/255, green: 212/255, blue: 198/255, alpha: 1.0)
let barBackGroundColor = UIColor(red: 56/255, green: 212/255, blue: 198/255, alpha: 1.0)
let statusColor = UIColor.whiteColor()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UIAlertViewDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        //[NSThread .sleepForTimeInterval(2.0)]
        //Set Color of navbar
        
        UINavigationBar.appearance().barTintColor = barBackGroundColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().barStyle = UIBarStyle.BlackTranslucent
        
        window?.tintColor = themeColor
        
        //Parse api implemented here to track opening of the push notifications.
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        //Implementation of the parse clientKey and application id, to have app communicate with the Parse Api
        Parse.setApplicationId("kGgp5YCD4oCjzF0O3ezllZFuAKjxUIHMLFQXJUB4", clientKey: "gJ9veWkVzmZLxgEs3TVr1muZ5D8iHrLtMKJsFnqY")
        
        
        // Register for Push Notifications
        //We register the Push Notifications UI sounds, also UI Badge to show notifications on the app icon
        let userNotificationTypes:UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let settings:UIUserNotificationSettings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced
            // in iOS 7). In that case, we skip tracking here to avoid double
            // counting the app-open.
            let oldPushHandlerOnly = !self.respondsToSelector(Selector("application:didReceiveRemoteNotification:fetchCompletionHandler:"))
            let noPushPayload: AnyObject? = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey]
            if oldPushHandlerOnly || noPushPayload != nil {
                PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
            }
        }
        
        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        // Store the deviceToken in the current installation and save it to Parse.
        let currentInstallation: PFInstallation = PFInstallation.currentInstallation()
        currentInstallation.setDeviceTokenFromData(deviceToken)
        currentInstallation.channels = [ "global" ]
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            currentInstallation.save()
            return
        })
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        PFPush.handlePush(userInfo)
        if application.applicationState == UIApplicationState.Inactive {
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
        }
        
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        let currentInstallation = PFInstallation.currentInstallation()
        if currentInstallation.badge != 0 {
            currentInstallation.badge = 0
            currentInstallation.saveEventually()
        }
        
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

