//
//  DetailView.swift
//  TWL
//
//  Created by Havic on 2/18/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//

import UIKit


class DetailView: UIViewController {
  var strArticleUrl: NSString = NSString()
  @IBOutlet weak var myWebView: UIWebView!
  
  override func viewDidLoad(){
    let articleUrl = NSURL(string: strArticleUrl as String)
    let request = NSURLRequest(URL: articleUrl!)
    myWebView.loadRequest(request)
  }
}

