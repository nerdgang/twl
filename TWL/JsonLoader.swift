//  JsonLoader.swift
//  TWL
//
//  Created by Havic on 3/2/15.
//  Copyright (c) 2015 Havic. All rights reserved.
//

import UIKit

//Custom web services class to download the jSon information, to store in the table.
protocol reloadJsonData{
    func reloadTableInfo()
}

class JsonLoader: NSObject {
  
    var blogPostArray = NSMutableArray()
    var blogImageUrl = NSURL?()
    var reloadJson = reloadJsonData?()
    
    //Method for connection to the custom jSON API server, to download data.
    
    /**
    
    Json Loader Method the downloading of json and the values being passed in from the json Object
    
    
    - parameter dataTask: -> shared session object dataTaskWithUrl(NSURL!,{ (NSData, NSResponse, NSError)} )
    
    
    - returns: The blogArray which is created from the info we temporaily stored it in tempArr
    
    */
    
    func loadThatJson(){
        let blogUrl: NSURL = NSURL(string: "http://techwelike.com/api/get_posts/?count=20/")!
        let session = NSURLSession.sharedSession()
        let dataTask:NSURLSessionDataTask = session.dataTaskWithURL(blogUrl, completionHandler: { (data, response, error) -> Void in
            let jsonDic:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)) as! NSDictionary
            let tempArr = jsonDic.objectForKey("posts") as! NSArray
            
            for objectData in tempArr as! [NSDictionary]{
                let blogObjects = BlogObject(jsonDic: objectData)
                self.blogPostArray.addObject(blogObjects)
            }
          
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.reloadJson?.reloadTableInfo()
            })
        })
        
        dataTask.resume()
  }
}
